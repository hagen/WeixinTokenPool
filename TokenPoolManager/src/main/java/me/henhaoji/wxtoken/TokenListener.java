package me.henhaoji.wxtoken;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
/**
 * Token监听器
 * 负责对Timer的注入以及关闭
 * Service中没有提供关闭Timer的接口[但可以得到Timer]
 * 
 * @author Hagen
 * 
 * */
public class TokenListener implements ServletContextListener {

	TokenTimer timer = null;
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		if(timer!=null)
		{
			timer.close();
		}
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		TokenService service = TokenService.getInstance();
		timer = new TokenTimer(service);
		service.setTimer(timer);
		timer.start();
		
	}

}
