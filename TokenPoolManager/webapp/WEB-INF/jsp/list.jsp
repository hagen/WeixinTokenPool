<%@ page 
language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" isELIgnored="false"%><%@
taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%><%@
taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%><%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%><!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<c:if test="${!empty msg }">
<font color="red">${msg }</font>
</c:if>

<table border="1">
<tr>
	<td>appId</td>
	<td>appSecret<td>
	<td>token</td>
	<td>expire_in</td>
	<td>expire</td>
	<td>updateTime</td>
	<td>userId</td>
	<td>remark</td>
	<td>operation</td>
</tr><c:forEach var="token" items="${requestScope.list }"><tr>
			<td>${token.id }</td>
			<td>${secMap[token.id] }</td>
				<td></td>
			<td>${tmpMap[token.id] }</td>
			<td>${token.expires_in }</td>
			<td>${token.expires }</td>
			<td><fmt:formatDate value="${dateMap[token.id] }" pattern="yyyy/MM/dd HH:mm:ss"/></td>
			<td>${token.userId }</td>
			<td>s${token.remark }</td>
			<td><a href='<c:url value="/refreshByUserId"/>?userId=${token.userId }'>refresh</a> 
				<a href='<c:url value="/getTokenByForce"/>?userId=${token.userId }'>force</a>
				<a href='<c:url value="/delToken"/>?appId=${token.id }'>delete</a>
				</td>
		</tr>
	</c:forEach>
</table>
<input type="button" value="添加" onclick='javascript:location.href="<c:url value="/add"/>"'>
<input type="button" value="刷新全部" onclick='javascript:location.href="<c:url value="/refreshAll"/>"'>
<input type="button" value="强制刷新全部" onclick='javascript:location.href="<c:url value="/refreshAllByForce"/>"'>
定时器状态：
<c:if test="${!empty timer }">已开启 
	<c:if test="${timer.valid }">
	已运行 ,距离下次刷新有：${timer.interval-timer.i }秒  <input type="button" value="暂停" onclick='javascript:location.href="<c:url value="/stopTimer"/>"'>
	</c:if>
	<c:if test="${!timer.valid }">未运行  <input type="button" value="运行" onclick='javascript:location.href="<c:url value="/startTimer"/>"'></c:if>
	
</c:if>
<c:if test="${empty timer }">
	  <input type="button" value="初始化定时器" onclick='javascript:location.href="<c:url value="/initTimer"/>"'>
</c:if>

</body>
</html>