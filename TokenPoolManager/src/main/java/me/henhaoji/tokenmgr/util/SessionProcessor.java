package me.henhaoji.tokenmgr.util;

import org.hibernate.Session;

public interface SessionProcessor {
	public void process(Session session);
}
