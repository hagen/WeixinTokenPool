package me.henhaoji.wxtoken;
/**
 * Token定时刷新器
 * run 定时器运行
 * valid 定时器运行方法体
 * interval 定时器刷新间隔
 * 
 * @author Hagen
 * 
 * 
 * */
public class TokenTimer extends Thread {

	TokenService service;
	boolean run=false;
	boolean valid=false;
	private int interval=1800;
	int i;
	TokenTimer(TokenService _service)
	{
		service = _service;
	}
	public void run()
	{
		run=true;
		i=0;
		while(run)
		{
			if(i==interval)
			{
				i=0;
				if(valid&&service!=null)
				{
					service.refreshAll();
				}
			}
			sleeps(1000);
			i++;
		}
	}
	
	public void sleeps(long time)
	{
		try {
			Thread.sleep(time);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public boolean isRun() {
		return run;
	}
	public void setRun(boolean run) {
		this.run = run;
	}
	public void close()
	{
		setRun(false);
	}
	public boolean isValid() {
		return valid;
	}
	public void setValid(boolean valid) {
		this.valid = valid;
	}
	public int getInterval() {
		return interval;
	}
	public void setInterval(int interval) {
		this.interval = interval;
	}
	public int getI() {
		return i;
	}
	public void setI(int i) {
		this.i = i;
	}
	
}
