package me.henhaoji.wxtoken;

import java.io.Serializable;
import java.util.List;







import me.henhaoji.tokenmgr.util.SessionProcessor;
import me.henhaoji.tokenmgr.util.SessionUtility;

import org.hibernate.Session;
import org.hibernate.Transaction;


public class BaseDao<T> {
	
	public boolean create(T obj)
	{
		Session session = SessionUtility.getSessionFactory().getCurrentSession();
		try
		{
		Transaction trx=session.beginTransaction();
		session.persist(obj);
		trx.commit();
		return true;
		}catch(Exception e)
		{
			session.getTransaction().rollback();
			return false;
		}
	}
	public boolean createOrUpdate(T obj)
	{
		Session session  =SessionUtility.getSessionFactory().getCurrentSession();
		try
		{
		Transaction trx=session.beginTransaction();
		session.saveOrUpdate(obj);
		trx.commit();
		return true;
		}catch(Exception e)
		{
			session.getTransaction().rollback();
			return false;
		}
	}
	public boolean update(T obj)
	{
		Session session  =SessionUtility.getSessionFactory().getCurrentSession();
		try
		{
		Transaction trx=session.beginTransaction();
		session.update(obj);
		trx.commit();
		return true;
		}catch(Exception e)
		{
			session.getTransaction().rollback();
			return false;
		}
	}
	public boolean delete(T obj)
	{
		Session session  =SessionUtility.getSessionFactory().getCurrentSession();
		try
		{
		Transaction trx=session.beginTransaction();
		session.delete(obj);
		trx.commit();
		return true;
		}catch(Exception e)
		{
			session.getTransaction().rollback();
			return false;
		}
	}
	@SuppressWarnings("unchecked")
	public T find(Class<? extends T> clazz,Serializable id)
	{
		Session session  =SessionUtility.getSessionFactory().getCurrentSession();
		try
		{
		session.beginTransaction();
		//session.delete(obj);
		return (T)session.get(clazz, id);
		//trx.commit();
		}
		finally{
			session.getTransaction().commit();
			}
	}
	
	@SuppressWarnings("unchecked")
	public List<T> listSql(Class<? extends T> clazz,String sql)
	{
		Session session = SessionUtility.getSessionFactory().getCurrentSession();
		try
		{
			session.beginTransaction();
			//return session.createQuery(hql).list();
			return session.createSQLQuery(sql).addEntity(clazz).list();
		}
		finally
		{
			session.getTransaction().commit();
			//session.close();
		}
	}
	@SuppressWarnings("unchecked")
	public List<T> listSql(Class<? extends T> clazz,String sql,int start,int pagelistnum)
	{
		Session session = SessionUtility.getSessionFactory().getCurrentSession();
		try
		{
			session.beginTransaction();
			//return session.createQuery(hql).list();
			return session.createSQLQuery(sql)
					.addEntity(clazz)
					.setFirstResult(start)
					.setMaxResults(pagelistnum)
					.list();
		}
		finally
		{
			session.getTransaction().commit();
			//session.close();
		}
	}
	@SuppressWarnings("unchecked")
	public List<T> listHql(String hql)
	{
		Session session = SessionUtility.getSessionFactory().getCurrentSession();
		try
		{
			session.beginTransaction();
			return session.createQuery(hql).list();
			//return session.createSQLQuery(sql).list();
		}
		finally
		{
			session.getTransaction().commit();
			//session.close();
		}
	}
	@SuppressWarnings("unchecked")
	public List<T> listHql(String hql,int start,int pagelistnum)
	{
		Session session = SessionUtility.getSessionFactory().getCurrentSession();
		try
		{
			session.beginTransaction();
			return session.createQuery(hql)
					.setFirstResult(start)
					.setMaxResults(pagelistnum)
					.list();
			//return session.createSQLQuery(sql).list();
		}
		finally
		{
			session.getTransaction().commit();
			//session.close();
		}
	}
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<T> list( Class clazz,int start,int pagelistnum)
	{
		Session session = SessionUtility.getSessionFactory().getCurrentSession();
		try
		{
			session.beginTransaction();
			return session.createQuery("from "+clazz.getName()+" ")
					.setFirstResult(start)
					.setMaxResults(pagelistnum)
					.list();
			//return session.createSQLQuery(sql).list();
		}
		finally
		{
			session.getTransaction().commit();
			//session.close();
		}
	}
	public int uniqueResult(String hql)
	{
		Session session = SessionUtility.getSessionFactory().getCurrentSession();
		try
		{
			session.beginTransaction();
			Number num = (Number)session.createQuery(hql).uniqueResult();
			//return session.createSQLQuery(sql).list();
			return num.intValue();
		}
		finally
		{
			session.getTransaction().commit();
			//session.close();
		}
	}
	public int uniqueResultSql(String sql)
	{
		Session session = SessionUtility.getSessionFactory().getCurrentSession();
		try
		{
			session.beginTransaction();
			Number num = (Number)session.createSQLQuery(sql).uniqueResult();
			//return session.createSQLQuery(sql).list();
			return num.intValue();
		}
		finally
		{
			session.getTransaction().commit();
			//session.close();
		}
	}
	public void template(SessionProcessor sp)
	{
		Session session = SessionUtility.getSessionFactory().getCurrentSession();
		try
		{
			session.beginTransaction();
			sp.process(session);
			//return session.createSQLQuery(sql).list();
		}
		finally
		{
			session.getTransaction().commit();
			//session.close();
		}
	}
	
}
